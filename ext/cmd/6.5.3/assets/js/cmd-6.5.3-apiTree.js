DocsApp.apiTree = {
    "API": [
        {
            "name": "Cmd",
            "text": "Cmd",
            "navTreeName": "api",
            "id": "Cmd",
            "leaf": false,
            "idSuffix": "",
            "iconCls": "class-type fa fa-cube",
            "children": [
                {
                    "name": "app",
                    "text": "app",
                    "navTreeName": "api",
                    "id": "Cmd.app",
                    "leaf": false,
                    "idSuffix": "",
                    "iconCls": "fa fa-folder-o",
                    "children": [
                        {
                            "name": "json",
                            "text": "json",
                            "navTreeName": "api",
                            "id": "Cmd.app.json",
                            "leaf": false,
                            "idSuffix": "",
                            "iconCls": "fa fa-folder-o",
                            "children": [
                                {
                                    "name": "packager",
                                    "text": "packager",
                                    "navTreeName": "api",
                                    "id": "Cmd.app.json.packager",
                                    "leaf": false,
                                    "idSuffix": "",
                                    "iconCls": "fa fa-folder-o",
                                    "children": [
                                        {
                                            "name": "Cordova",
                                            "text": "Cordova",
                                            "navTreeName": "api",
                                            "id": "Cmd.app.json.packager.Cordova",
                                            "leaf": true,
                                            "idSuffix": "",
                                            "access": "public",
                                            "iconCls": "class-type fa fa-cube",
                                            "href": "api/Cmd.app.json.packager.Cordova.html"
                                        },
                                        {
                                            "name": "Phonegap",
                                            "text": "Phonegap",
                                            "navTreeName": "api",
                                            "id": "Cmd.app.json.packager.Phonegap",
                                            "leaf": true,
                                            "idSuffix": "",
                                            "access": "public",
                                            "iconCls": "class-type fa fa-cube",
                                            "href": "api/Cmd.app.json.packager.Phonegap.html"
                                        }
                                    ],
                                    "access": "public"
                                },
                                {
                                    "name": "progressive",
                                    "text": "progressive",
                                    "navTreeName": "api",
                                    "id": "Cmd.app.json.progressive",
                                    "leaf": false,
                                    "idSuffix": "",
                                    "iconCls": "fa fa-folder-o",
                                    "children": [
                                        {
                                            "name": "Manifest",
                                            "text": "Manifest",
                                            "navTreeName": "api",
                                            "id": "Cmd.app.json.progressive.Manifest",
                                            "leaf": true,
                                            "idSuffix": "",
                                            "access": "public",
                                            "iconCls": "class-type fa fa-cube",
                                            "href": "api/Cmd.app.json.progressive.Manifest.html"
                                        },
                                        {
                                            "name": "Progressive",
                                            "text": "Progressive",
                                            "navTreeName": "api",
                                            "id": "Cmd.app.json.progressive.Progressive",
                                            "leaf": true,
                                            "idSuffix": "",
                                            "access": "public",
                                            "iconCls": "class-type fa fa-cube",
                                            "href": "api/Cmd.app.json.progressive.Progressive.html"
                                        },
                                        {
                                            "name": "SerivceWorker",
                                            "text": "SerivceWorker",
                                            "navTreeName": "api",
                                            "id": "Cmd.app.json.progressive.SerivceWorker",
                                            "leaf": true,
                                            "idSuffix": "",
                                            "access": "private",
                                            "iconCls": "class-type fa fa-cube",
                                            "href": "api/Cmd.app.json.progressive.SerivceWorker.html"
                                        }
                                    ],
                                    "access": "public"
                                },
                                {
                                    "name": "AppCache",
                                    "text": "AppCache",
                                    "navTreeName": "api",
                                    "id": "Cmd.app.json.AppCache",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "public",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.app.json.AppCache.html"
                                },
                                {
                                    "name": "Fashion",
                                    "text": "Fashion",
                                    "navTreeName": "api",
                                    "id": "Cmd.app.json.Fashion",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "public",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.app.json.Fashion.html"
                                },
                                {
                                    "name": "Manifest",
                                    "text": "Manifest",
                                    "navTreeName": "api",
                                    "id": "Cmd.app.json.Manifest",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "public",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.app.json.Manifest.html"
                                },
                                {
                                    "name": "Output",
                                    "text": "Output",
                                    "navTreeName": "api",
                                    "id": "Cmd.app.json.Output",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "public",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.app.json.Output.html"
                                }
                            ],
                            "access": "public"
                        }
                    ],
                    "access": "public"
                },
                {
                    "name": "auto",
                    "text": "auto",
                    "navTreeName": "api",
                    "id": "Cmd.auto",
                    "leaf": false,
                    "idSuffix": "",
                    "iconCls": "fa fa-folder-o",
                    "children": [
                        {
                            "name": "Dependency",
                            "text": "Dependency",
                            "navTreeName": "api",
                            "id": "Cmd.auto.Dependency",
                            "leaf": true,
                            "idSuffix": "",
                            "access": "private",
                            "iconCls": "class-type fa fa-cube",
                            "href": "api/Cmd.auto.Dependency.html"
                        }
                    ],
                    "access": "private"
                },
                {
                    "name": "codebase",
                    "text": "codebase",
                    "navTreeName": "api",
                    "id": "Cmd.codebase",
                    "leaf": false,
                    "idSuffix": "",
                    "iconCls": "fa fa-folder-o",
                    "children": [
                        {
                            "name": "json",
                            "text": "json",
                            "navTreeName": "api",
                            "id": "Cmd.codebase.json",
                            "leaf": false,
                            "idSuffix": "",
                            "iconCls": "fa fa-folder-o",
                            "children": [
                                {
                                    "name": "Asset",
                                    "text": "Asset",
                                    "navTreeName": "api",
                                    "id": "Cmd.codebase.json.Asset",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "public",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.codebase.json.Asset.html"
                                },
                                {
                                    "name": "Compressor",
                                    "text": "Compressor",
                                    "navTreeName": "api",
                                    "id": "Cmd.codebase.json.Compressor",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "public",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.codebase.json.Compressor.html"
                                },
                                {
                                    "name": "JS",
                                    "text": "JS",
                                    "navTreeName": "api",
                                    "id": "Cmd.codebase.json.JS",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "public",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.codebase.json.JS.html"
                                },
                                {
                                    "name": "Language",
                                    "text": "Language",
                                    "navTreeName": "api",
                                    "id": "Cmd.codebase.json.Language",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "public",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.codebase.json.Language.html"
                                },
                                {
                                    "name": "Manifest",
                                    "text": "Manifest",
                                    "navTreeName": "api",
                                    "id": "Cmd.codebase.json.Manifest",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "public",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.codebase.json.Manifest.html"
                                },
                                {
                                    "name": "Output",
                                    "text": "Output",
                                    "navTreeName": "api",
                                    "id": "Cmd.codebase.json.Output",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "public",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.codebase.json.Output.html"
                                },
                                {
                                    "name": "Resource",
                                    "text": "Resource",
                                    "navTreeName": "api",
                                    "id": "Cmd.codebase.json.Resource",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "public",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.codebase.json.Resource.html"
                                },
                                {
                                    "name": "Sass",
                                    "text": "Sass",
                                    "navTreeName": "api",
                                    "id": "Cmd.codebase.json.Sass",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "public",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.codebase.json.Sass.html"
                                }
                            ],
                            "access": "public"
                        }
                    ],
                    "access": "public"
                },
                {
                    "name": "package",
                    "text": "package",
                    "navTreeName": "api",
                    "id": "Cmd.package",
                    "leaf": false,
                    "idSuffix": "",
                    "iconCls": "fa fa-folder-o",
                    "children": [
                        {
                            "name": "json",
                            "text": "json",
                            "navTreeName": "api",
                            "id": "Cmd.package.json",
                            "leaf": false,
                            "idSuffix": "",
                            "iconCls": "fa fa-folder-o",
                            "children": [
                                {
                                    "name": "Manifest",
                                    "text": "Manifest",
                                    "navTreeName": "api",
                                    "id": "Cmd.package.json.Manifest",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "public",
                                    "iconCls": "singleton-type fa fa-cube",
                                    "href": "api/Cmd.package.json.Manifest.html"
                                }
                            ],
                            "access": "public"
                        }
                    ],
                    "access": "public"
                },
                {
                    "name": "template",
                    "text": "template",
                    "navTreeName": "api",
                    "id": "Cmd.template",
                    "leaf": false,
                    "idSuffix": "",
                    "iconCls": "fa fa-folder-o",
                    "children": [
                        {
                            "name": "json",
                            "text": "json",
                            "navTreeName": "api",
                            "id": "Cmd.template.json",
                            "leaf": false,
                            "idSuffix": "",
                            "iconCls": "fa fa-folder-o",
                            "children": [
                                {
                                    "name": "ExclusionValidator",
                                    "text": "ExclusionValidator",
                                    "navTreeName": "api",
                                    "id": "Cmd.template.json.ExclusionValidator",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "private",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.template.json.ExclusionValidator.html"
                                },
                                {
                                    "name": "InclusionValidator",
                                    "text": "InclusionValidator",
                                    "navTreeName": "api",
                                    "id": "Cmd.template.json.InclusionValidator",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "private",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.template.json.InclusionValidator.html"
                                },
                                {
                                    "name": "ListValidator",
                                    "text": "ListValidator",
                                    "navTreeName": "api",
                                    "id": "Cmd.template.json.ListValidator",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "private",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.template.json.ListValidator.html"
                                },
                                {
                                    "name": "Manifest",
                                    "text": "Manifest",
                                    "navTreeName": "api",
                                    "id": "Cmd.template.json.Manifest",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "private",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.template.json.Manifest.html"
                                },
                                {
                                    "name": "Parameter",
                                    "text": "Parameter",
                                    "navTreeName": "api",
                                    "id": "Cmd.template.json.Parameter",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "private",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.template.json.Parameter.html"
                                },
                                {
                                    "name": "PresenceValidator",
                                    "text": "PresenceValidator",
                                    "navTreeName": "api",
                                    "id": "Cmd.template.json.PresenceValidator",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "private",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.template.json.PresenceValidator.html"
                                },
                                {
                                    "name": "RangeValidator",
                                    "text": "RangeValidator",
                                    "navTreeName": "api",
                                    "id": "Cmd.template.json.RangeValidator",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "private",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.template.json.RangeValidator.html"
                                },
                                {
                                    "name": "Validator",
                                    "text": "Validator",
                                    "navTreeName": "api",
                                    "id": "Cmd.template.json.Validator",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "private",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.template.json.Validator.html"
                                }
                            ],
                            "access": "private"
                        }
                    ],
                    "access": "private"
                },
                {
                    "name": "workspace",
                    "text": "workspace",
                    "navTreeName": "api",
                    "id": "Cmd.workspace",
                    "leaf": false,
                    "idSuffix": "",
                    "iconCls": "fa fa-folder-o",
                    "children": [
                        {
                            "name": "json",
                            "text": "json",
                            "navTreeName": "api",
                            "id": "Cmd.workspace.json",
                            "leaf": false,
                            "idSuffix": "",
                            "iconCls": "fa fa-folder-o",
                            "children": [
                                {
                                    "name": "Framework",
                                    "text": "Framework",
                                    "navTreeName": "api",
                                    "id": "Cmd.workspace.json.Framework",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "public",
                                    "iconCls": "class-type fa fa-cube",
                                    "href": "api/Cmd.workspace.json.Framework.html"
                                },
                                {
                                    "name": "Manifest",
                                    "text": "Manifest",
                                    "navTreeName": "api",
                                    "id": "Cmd.workspace.json.Manifest",
                                    "leaf": true,
                                    "idSuffix": "",
                                    "access": "public",
                                    "iconCls": "singleton-type fa fa-cube",
                                    "href": "api/Cmd.workspace.json.Manifest.html"
                                }
                            ],
                            "access": "public"
                        }
                    ],
                    "access": "public"
                }
            ],
            "href": "api/Cmd.html",
            "access": "public"
        },
        {
            "name": "Array",
            "text": "Array",
            "navTreeName": "api",
            "id": "Array",
            "leaf": true,
            "idSuffix": "",
            "access": "public",
            "iconCls": "class-type fa fa-cube",
            "href": "api/Array.html"
        },
        {
            "name": "Boolean",
            "text": "Boolean",
            "navTreeName": "api",
            "id": "Boolean",
            "leaf": true,
            "idSuffix": "",
            "access": "public",
            "iconCls": "class-type fa fa-cube",
            "href": "api/Boolean.html"
        },
        {
            "name": "Date",
            "text": "Date",
            "navTreeName": "api",
            "id": "Date",
            "leaf": true,
            "idSuffix": "",
            "access": "public",
            "iconCls": "class-type fa fa-cube",
            "href": "api/Date.html"
        },
        {
            "name": "Function",
            "text": "Function",
            "navTreeName": "api",
            "id": "Function",
            "leaf": true,
            "idSuffix": "",
            "access": "public",
            "iconCls": "class-type fa fa-cube",
            "href": "api/Function.html"
        },
        {
            "name": "Number",
            "text": "Number",
            "navTreeName": "api",
            "id": "Number",
            "leaf": true,
            "idSuffix": "",
            "access": "public",
            "iconCls": "class-type fa fa-cube",
            "href": "api/Number.html"
        },
        {
            "name": "Object",
            "text": "Object",
            "navTreeName": "api",
            "id": "Object",
            "leaf": true,
            "idSuffix": "",
            "access": "public",
            "iconCls": "class-type fa fa-cube",
            "href": "api/Object.html"
        },
        {
            "name": "RegExp",
            "text": "RegExp",
            "navTreeName": "api",
            "id": "RegExp",
            "leaf": true,
            "idSuffix": "",
            "access": "public",
            "iconCls": "class-type fa fa-cube",
            "href": "api/RegExp.html"
        },
        {
            "name": "String",
            "text": "String",
            "navTreeName": "api",
            "id": "String",
            "leaf": true,
            "idSuffix": "",
            "access": "public",
            "iconCls": "class-type fa fa-cube",
            "href": "api/String.html"
        }
    ]
}